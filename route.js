'use strict';

var myColCtrl = require('./controllers/myCollectionCtrl');

var Routes = [
    { path: '/getMyCollection', method: 'GET', handler: myColCtrl.getMyCollection },
    { path: '/InsertMyCollection', method: 'POST', handler: myColCtrl.InsertMyCollection },
    { path: '/UpdateMyCollection', method: 'POST', handler: myColCtrl.UpdateMyCollection },
    { path: '/DeleteMyCollection', method: 'POST', handler: myColCtrl.DeleteMyCollection }
];

module.exports = Routes;
