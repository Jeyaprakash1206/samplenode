const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

// Connection URL
const url = 'mongodb://localhost:27017';

// Database Name
const dbName = 'testdb';
let db = {};
// Use connect method to connect to the server
MongoClient.connect(url, function (err, client) {
    assert.equal(null, err);
    console.log("Connected successfully to server");

    db = client.db(dbName);

    // client.close();
});
var InsertMyCollection = (request, reply) => new Promise((resolve, reject) => {
    var data = {
        "name": request.payload.name,
        "age": request.payload.age
    }
    const collection = db.collection('mycollection');
    // Insert some documents
    collection.insertOne(data, function (err, result) {
        if (!err && result.result.n ===1) {
            console.log("Inserted Successfully");
            return resolve({ Status: true, Message: "Success" });
        }
        else {
            console.log('Error Occured');
            return resolve({ Status: false, Message: "Failure" });
        }
    });
})
var UpdateMyCollection = (request, reply) => new Promise((resolve, reject) => {
    // Get the documents collection
    const collection = db.collection('mycollection');
    const search = {
        "name": request.payload.name
    }
    const update = {
        age: request.payload.age
    }
    collection.updateOne(search
        , { $set: update }, function (err, result) {
            if (!err && result.result.n ===1) {
                console.log("Updated the document with the field a equal to 2");
                return resolve(result);
            }
            else {
                console.log('Error Occured');
                return resolve({ Status: false, Message: "Failure" });
            }
        });
})
var DeleteMyCollection = (request, reply) => new Promise((resolve, reject) => {
    // Get the documents collection
    const collection = db.collection('mycollection');
    const search = {
        "name": request.payload.name
    }
    collection.deleteOne(search, function (err, result) {
        if (!err && result.result.n ===1) {
            resolve(result);
        }
        else {
            console.log('Error Occured');
            return resolve({ Status: false, Message: "Failure" });
        }
    });
})
var getMyCollection = () => new Promise((resolve, reject) => {
    // Get the documents collection
    const collection = db.collection('mycollection');
    // Find some documents
    collection.find({}).toArray(function (err, docs) {
        if (!err) {
        assert.equal(err, null);
        console.log("Found the following records");
        console.log(docs)
        return resolve({ Status: true, Message: "Success", data: docs });
        }
        else {
            console.log('Error Occured');
            return resolve({ Status: false, Message: "Failure" });
        }
    });

})
module.exports = {
    getMyCollection,
    InsertMyCollection,
    UpdateMyCollection,
    DeleteMyCollection
}